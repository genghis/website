
DEST ?= public
PO_DIR = po

all: build

init:
	git submodule init && git submodule update --recursive

build: init locale-build
	hugo --destination $(DEST)

server: init
	hugo server --buildDrafts --buildFuture --disableFastRender

locale-new:
	@echo "Be sure to set the NEW_LANG variable."
	if [ -z "$(NEW_LANG)" ]; then exit 1; fi
	cp $(PO_DIR)/content.pot $(PO_DIR)/content.$(NEW_LANG)\.po
	@echo "Lang '$(NEW_LANG)' added. Edit \"$(PO_DIR)/content.$(NEW_LANG).po\"."

locale-update:
	./generate-translations.sh --no-translations

locale-build:
	./generate-translations.sh --no-update

clean:
	./generate-translations.sh --rm-translations

.PHONY: all init build serve locale-new locale-update locale-build
