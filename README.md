# JoinJabber website

Based on [Compose theme](https://github.com/onweru/compose). Find the documentation [here](https://docs.neuralvibes.com/docs/compose/install-theme/).

All text in the `content` folder is [Creative Commons CC-by-SA](https://creativecommons.org/licenses/by-sa/4.0/) licensed.

## Contributions

Contributions, especially tutorials and translations, are highly appreciated. This website uses plain-text files with simple [Markdown formatting](https://www.markdownguide.org/cheat-sheet/). Ready-made PRs or patch files for this repository are perfect and can be made quite easily on the Codeberg web-interface.
But if you send us a simple markdown formatted text file with your changes that is also fine.

You can join our project channel via a Jabber client to discuss any contributions with us: [project@joinjabber.org](xmpp:project@joinjabber.org?join) or you can also use our [simple webclient](https://chat.joinjabber.org/#/guest?join=project); no registration required.

Please note that all contributions should be licensed under the [Creative Commons CC-by-SA license](https://creativecommons.org/licenses/by-sa/4.0/) or a license compatible to it.

## Running a local copy of the site for editing

After cloning or downloading the git repository (and ensuring you have the [Hugo static site builder](https://gohugo.io/) installed) open a terminal and change to the `website-hugo` directory. Run `make init` to pull the submodule for the theme. Then you can start an interactive version of the site by running `make server`. The terminal output will show a link you can open with any web-browser. Changes to the files will be automatically reflected on this live-updating local version of the website.

## Helping with translations

We are comitted to providing our website in multiple languages, however keeping up with changes and adding additional languages is quite a challenge for our small team of voluteers. So please consider helping out!

### Translating online

Translations are made available on [Codeberg's Weblate instance](https://translate.codeberg.org/projects/joinjabber/website/).

All strings are being served from the same template that contains the whole website. You may want to filter based on language and location of the file (article) you want to translate using the following weblate search query: `language:fr location:content/_index.en.md`.

It is possible to add a new language directly from Weblate but please [contact us (web)](https://chat.joinjabber.org/#/guest?join=chat) ([xmpp](xmpp:chat@joinjabber.org?join)) when doing so.

### Translating locally

Translations are available in the `po` folder at the root of the repository. Look for `.<lang>.po` files.

To update translation files with new (english) content, run `make locale-update`. To update the content folder that will be served on the website with new translations, run `make locale-build`. If a language is managed by our scripts, that is every language that has a `.po` files, and is not listed in `disable-languages` file, then any article that is not translated up to at least 80% will not be generated and will be deleted if it exists.

To add a new language, `cp po/content.pot po/content.<lang>.pot`, and please contact us if you do.

### Translating links with hugo ref/relref

Usage of `{{< ref "link" >}}` or `{{< relref "link" >}}` in translations may
fail if the linked page doesn't exist yet (or not translated enough). In this
case, remove the link. Periodic checks that links are properly translated
should happen.

When translating from weblate, add a "missing-link" label. When translating
locally, leave a `#. Missing link` comment. Unfortunately it doesn't seem like
weblate provides a way to leave comments in PO files.
