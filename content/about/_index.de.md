---
title: "Über JoinJabber"
---

JoinJabber ist ein informelles internationales Kollektiv aus Menschen mit diversem Hintergrund und dem gemeinsamen Ziel Jabber/XMPP, ein freies und föderiertes Netzwerk mit dem Schwerpunkt auf Echtzeitchat, populärer zu machen.

Leider haben wir diesen Bereich noch nicht kompett übersetzt ([Hilfe dabei gerne gesehen](https://codeberg.org/joinjabber/website-hugo#helping-with-translations)). Das englische Original [findest du hier](/about/).
