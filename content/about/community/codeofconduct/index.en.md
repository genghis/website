---
title: "Code of Conduct"
---

By using our services you agree to adhere to our Code of Conduct:

- No discrimination about race, sex, gender, ability, etc.
- Focus on behavior not people when pointing out an issue.
- Alt-right speech and trolling will not be tolerated.

TODO: add more detailed CoC.


