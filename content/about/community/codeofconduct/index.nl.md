---
title: "Gedragscode"
---

Door gebruik te maken van onze diensten ga je akkoord met onze gedragscode:

- Geen discriminatie over ras, sekse, geslacht, handicaps, enz.
- Focus op gedrag en niet op mensen wanneer je een probleem aankaart.
- Alt-rightgeluiden en trollen worden niet getolereerd.

TODO: meer gedetailleerde gedragscode toevoegen.
