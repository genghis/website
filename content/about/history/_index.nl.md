---
title: "Geschiedenis"
weight: 6
---

JoinJabber is begin 2021 opgericht als een informeel collectief. Je kunt hier meer over lezen in onze [aankondigingspost](/blog/announcement).

De notulen van onze vergaderingen staan in het Engels op [deze website](/about/history).