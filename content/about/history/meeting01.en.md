---
author: "JoinJabber"
tags: ["meeting","minutes","collective"]
title: "Meeting #1"
date: "2021-02-07"
---

This first meeting took place on [a pad](https://pad.lqdn.fr/p/joinjabber) on February 7 2021 17:00 GMT. A dozen people took part in it, and some points on our very long agenda had to be postponed for the next meeting after more than 4 hours of discussions

# Agenda {#agenda}

- [Goals for the JoinJabber project](#points-goals)
- [Retrospective: what's up so far](#points-retrospective)
- [Shared infrastructure](#points-infra)
- [Communication](#points-comm)
  - [Announcement](#points-comm-announcement)
  - [Translations](#points-comm-translations)
  - [Newsfeed](#points-comm-newsfeed)
  - [Social media](#points-comm-socialmedia)
  - [Chat](#points-comm-chat)
  - [Invitations](#points-comm-invitations)
- [Project working groups](#points-wg)
  - [Sysadmin](#points-wg-sysadmin)
  - [Website](#points-wg-website)
  - [Translations](#points-wg-translations)
  - [Bridging](#points-wg-bridging)
  - [Media](#points-wg-media)
- [Additional tasks](#points-tasks)
- [Postponed](#postponed)
  - [Recommended servers](#postponed-servers)
  - [Communication](#postponed-comm)
    - [Jabber or XMPP?](#postponed-comm-jabberxmpp)
    - [Upstream news to follow](#postponed-comm-upstreamnews)
    - [Useful resources](#postponed-comm-resources)
  - [Questions and answers](#postponed-questions)

# Points

In this section, we discuss individual points scheduled in the agenda.

## Goals for the JoinJabber project {#points-goals}

We are gathered today to form a new collective called JoinJabber. Some of the reasons we are here include:

- information about the Jabber ecosystem is fragmented and often outdated, which is confusing for new users ; many service providers (hosting cooperatives) have to spend their precious resources on maintaining documentation for end-users
- xmpp.org and the XMPP Standards Foundation (XSF) have a neutral position, which prevents them from recommending certain solutions and approaches
- there is no community hub to help and guide new users and server operators when trouble arise, or to facilitate and encourage discussions between end-users, and client/server developers; existing communities like jdev and jusers are not widely-known

For these reasons, we adopt the following goals for the JoinJabber collective:

- JoinJabber is a community dedicated to the concerns of end-users of the Jabber federated network
- JoinJabber promotes cultural diversity and international cooperation; we want to empower people to communicate without any form of oppression or barriers; any language is welcome in the community, although each language has its own channels (forums, chats…).
- Communication and decision-making within the JoinJabber collective takes place in English, and we encourage speakers of other languages to express their concerns via translations and/or delegates of their choice. The scheduled agenda for a meeting is published and translated in advance on a best-effort basis, and so are meeting minutes.
- JoinJabber is a free association, which promotes empowerment of individuals and communities; every person is free to express desires and concerns during our meetings, and we have no formal membership
- JoinJabber opposes toxic and oppressive behavior such as stalking, harrassment, and spamming; we will ban people harming our users and our community
- JoinJabber is not involved in any form of commercial activity: we may raise donations for JoinJabber infrastructure, advertise existing fundraisers for solutions we recommend (clients, servers, and server operators), or take part in fundraisers for new features of interest to end users
- JoinJabber promotes decentralized governance: we have no authority over any project, though we may choose to recommend or not certain projects depending on further-established criteria
- JoinJabber promotes inter-project cooperation and would like to encourage adoption of modern standards focused on the needs of end-users
- JoinJabber promotes decentralized Internet infrastructure: we do not want to become a big central server, but rather promote existing user-friendly servers and the selfhosting of new servers
- JoinJabber provides services and infrastructures for collective projects around the Jabber ecosystem, but does not provide services to end-users (such as Jabber accounts)
- JoinJabber provides support channels (instant chat and long-lived discussions) to help users of Jabber clients/servers; we do not want to replace existing support channels for specific projects, but would like to help lower their burden with simple matters; support is provided by volunteers on a best-effort basis
- JoinJabber promotes privacy as a fundamental human right, and as a pillar of popular power and autonomy; we take part in evaluating privacy of the Jabber ecosystem, and proposing recommendations for implementers, for server operators, and for end-users; the use of a nickname is recommended (though not mandatory) to participate in the JoinJabber collective
- JoinJabber studies user experience across the Jabber ecosystem, and collects end-user feedback; this feedback, aggregated from testimonies and field-studies (for example during install parties) can be used to propose recommendations for implementers and server operators
- JoinJabber promotes interoperability between Jabber and other free, decentralized networks; we can only stand up to the Internet giants if we are able to communicate across free solutions (such as IRC, Matrix, Fediverse..)
- JoinJabber promotes accessibility to all services, regardless of physiology (disabilities) and resource limitations (slow Internet access, low-end hardware, or lack of money).
- JoinJabber encourages reproducible builds for more reliable and trustworthy software. JoinJabber may provide software repositories to encourage quicker, secure updates for existing client/server software across operating systems.

All of these goals were adopted by soft consensus. Some had different phrasings proposed, and although not everyone could get their favorite approved, we made sure nobody had strong feelings against any adopted proposal. Reaching these conclusions took over two hours, but everyone appears to be glad of the result, and we are now recognizing ourselves as the JoinJabber collective with a defined set of goals.

While discussing certain points, it was prefered to avoid some expressions (eg. collaboration) which had negative connotations in some cultural contexts, or words which could be cryptic to some people (eg. autonomous). It was also that some points may be reformulated better or more precise in the future. In particular, proposals 9 and 10 (about decentralized Internet infrastructure) may be merged into a single proposal at the next meeting. Furthermore, some people proposed that goals and methods should be separate documents. There may also be a proposal for this at the next meeting.

## Retrospective: what's up so far {#points-retrospective}

From december 2020 until now, we have setup a website to list clients/servers and have been working on Ansible recipes for making our infrastructure understandable, reproducible and extensible. We also have started to communicate around the JoinJabber project to raise awareness of it: several client/server developers, as well as server operators, have now joined our multi-user chat and take part in discussions.

The website is well-documented, but has little translations (only English and French so far). The design is pretty minimalist, so the website will load fine for people across the planet (even on low-bandwidth links). It's not the prettiest so far, and could use help from a volunteer designer. The source for the [website](https://codeberg.org/joinjabber/website) and the [infrastructure](https://codeberg.org/joinjabber/infra) are linked on the [website](https://joinjabber.org/), and most services should be reachable over Tor via onion addresses. Both informations are available in the footer on every page. Our web forum also acts as a mailing-list, which is the reason Discourse was chosen to be our forum software, and not another solution.

The infrastructure currently lacks documentation, but this will be improved soon as part of a dedicated **Sysadmin Working Group**. Some roles are not finished yet (discourse, mailserver, jabberserver), and volunteer help is welcome.

## Shared infrastructure {#points-infra}

Currently, our infrastructure is:

- [a website](https://joinjabber.org/)
- a mailserver (for internal services like the forum)
- [a forum](https://forum.joinjabber.org/), which is some kind of mailing list as well (supports notifications and replies via email)
- a Jabber/XMPP multi-user chat (MUC) server

Additionally, we consider to provide additional services for the communities. We are not discussing how/when to deploy such services, but question whether we think they would be useful, if some volunteers ever show up to setup them. These services could be provided to Jabber-related projects:

- website/wiki/documentation hosting
- a translations service, powered by [Weblate](https://weblate.org/en/ )
- forums/mailinglists
- a bot/bridge to enable discussions across instant-messaging protocols (IRC-Matrix-Jabber), powered by [matterbridge](https://github.com/42wim/matterbridge) (or equivalent)
- chatbots subscribing to updates on software forges (Github/Gitlab/Gitea) to advertise changes in project chats
- chatrooms, powered by our Jabber server's MUC (multi-user chat)
- software repositories, to provide up-to-date software on different platforms (F-droid, APT, Docker..)

All the proposals above are adopted. Furthermore, some proposals did not reach a consensus and require more consideration in a future meeting:

- bridging instant-messaging protocols, as a service provided to existing communities unrelated to the Jabber ecosystem who do not have the knowledge/resources to deploy such solutions themselves
- bridging instant-messaging protocols to non-free services (like Discord)
- a software forge (like Gitea), whether as primary source or as mirroring service

## Communication {#points-comm}

### Announcement {#points-comm-announcement}

As our collective was just formed, we need to announce its creation publicly. A dedicated pad (public document) will be opened, and feedback will be collected as we go on the forums and on the chat. Everyone is welcome to voice ideas/concerns during this process, and we will not wait for the next meeting to publish the announcement.

### Translations {#points-comm-translations}

We want our community to be accessible and inclusive to people of all languages. It may be a concern that some services don't support right-to-left languages (it was not tested so far). Translations and all associated concerns are dealt with by the **Translations Working Group**. This group may also deal with outreach to communities not already covered by our translations.

### Newsfeed {#points-comm-newsfeed}

We'd like to setup a newsfeed where people can get articles in their language about topics of interest to us. This could take place on the forum, or in a dedicated service (Lemmy/Lobsters/Postmill). However, nobody appears to desire to maintain yet another service.

It would not exactly be [the XMPP planet](https://planet.xmpp.org/) because articles are curated manually and the whole content is not hosted on our newsfeed (we host links), and not exactly the [XMPP newsletter](https://xmpp.org/category/newsletter.html) because each linked tutorial/article would have its own forum post for gathering community discussion.

Anyone may contribute to this newsfeed in a dedicated category in the forums: [in English](https://forum.joinjabber.org/c/en/en-news/9), [in French](https://forum.joinjabber.org/c/fr/fr-news/12).

### Social media {#points-comm-socialmedia}

We would like to advertise our activities, and that of the broader Jabber ecosystem on the following platforms:

- Mastodon and the Fediverse (ActivityPub federation)
- Movim (Jabber/XMPP PubSub federation)
- as well as an RSS feed on our website

We would like our accounts on different social media to stay synchronized, so we need a dedicated client/bot for this. This client could also import replies from external media back to our website. A solution is desired and somebody is more than welcome to develop it. In the meantime RSS from the website, Mastodon and Movim are our reference social media feeds.

Other social media may be considered in the future: Diaspora, Hubzilla (ZOT protocol).. Twitter and Facebook have also been mentioned, but have overall received pretty negative feedback from the community because of their user-hostile policies.

A dedicated **Media Working Group** will handle communication on those platforms.

### Chat (instant messaging) {#points-comm-chat}

Our historic chatroom is at joinxmpp@chat.cluxia.eu. We have a new chat server available at joinjabber.org. The new room is called "chat" on the MUC. All conversations should now take place on [chat@joinjabber.org](xmpp:chat@joinjabber.org?join).

We additionally would like to bridge discussions on our chat with other channels:

- chat@joinxmpp.org
- #joinjabber on a popular matrix server: maybe matrix.org?
- #joinjabber on a popular IRC server: maybe OFTC, because Freenode is rather hostile to Tor users?

A **Bridging Working Group** will setup matterbridge (or an equivalent solution) before the next meeting, so people can participate from other networks. Additionally, it was suggested to setup [ConverseJS](https://conversejs.org/ ) on our server as a web client to join the chat.

### Invitations {#points-comm-invitations}

We would like to invite other collectives to join the discussion/collective. Proposals will be gathered on the forum/MUC until the next meeting, so we can send invitations a few days in advance.

# Project working groups {#points-wg}

In order to distribute tasks among the community, we may split into dedicated working groups. People interested in joining a working group are encouraged to declare it here. This list does not constitute formal membership of any kind and people may join/leave working groups at any time.

## Sysadmin {#points-wg-sysadmin}

Members: tofu, pep., Kris

The **Sysadmin Working Group** maintains the JoinJabber infrastructure. It is in charge of deploying new services, and ensuring the continuity of existing services. Almost all of the infrastructure is managed by a [declarative config file](https://codeberg.org/joinjabber/infra/src/branch/main/config.yml) managed by Ansible recipes available at [https://codeberg.org/joinjabber/infra](https://codeberg.org/joinjabber/infra).

**TODO:**

- some tasks are listed on [the repository's bugtracker](https://codeberg.org/joinjabber/infra/issues)
- write an Ansible recipe for the bridging solution the **Bridging Working Group** will come up with
- consider setting up [ConverseJS](https://conversejs.org/) for people to join our chatrooms and meetings from the web

## Website {#points-wg-website}

Members: tofu, wurstsalat

The **Website Working Group** maintains the [joinjabber.org](https://joinjabber.org/) website. It proposes new content, collects external contributions, translations elaborated by the **Translations Working Group**, and tries to improve the design of the website.

**TODO:**

- some tasks are listed on [the repository's bugtracker](https://codeberg.org/joinjabber/website/issues)
- open a microblogging section on the website, which the **Media Working Group** will publish to
- open a blog section, where announcements and meeting minutes will be published

## Translations {#points-wg-translations}

Members: pep. (fr), pitchum (fr)

The **Translations Working Group** keeps track of content to be translated, and coordinates translation teams across languages. It is also in charge of outreach to communities we currently don't have translations for.

**TODO:**

- translate these meeting minutes
- translate the announcement of the collective
- recruit more translators

## Bridging {#points-wg-bridging}

Members: Kris, tofu

The **Bridging Working Group** experiments with bridges and chatbots to connect different chatrooms together and enable people to work across chat protocols (IRC, Matrix, Jabber, etc).

**TODO:**

- experiment with [matterbridge](https://github.com/42wim/matterbridge), and potentially other solutions
- propose a solution for the **Sysadmin Working Group** to deploy

## Media {#points-wg-media}

Members:  Kris (Lemmy), pep. (Mastodon, Movim)

The **Media Working Group** is in charge of publishing content of interest to our microblog, Mastodon and Movim (for the moment). It also makes sure these different sources stay synchronized.

**TODO:**

- create accounts on Mastodon and Movim, and share credentials
- obtain credentials from the **Website Working Group** to publish on website in the microblogging section
- consider whether we should selfhost our own Movim instance, and if so get in touch with the **Sysadmin Working Group**

# Additional tasks {#points-tasks}

In additional to the tasks of specific working groups (detailed above), the following tasks should be dealt with before the next meeting, as a collective responsibility:

- publish this meeting's minutes
- publish the announcement
- start a poll for the next meeting's date
- prepare the pad for the next meeting, adding points we could not discuss today to the agenda
- list other collectives we'd like to invite to the meeting
- prepare a tutorial on how to organize meetings and participate to them

# Postponed {#postponed}

After more than four hours of meeting, we have decided to postpone some points for the next meeting. The notes below are not the result of collective discussion, but proposals on the agenda.

## Recommended servers {#postponed-servers}

What servers do we recommend? What is the criteria?
Example: https://invent.kde.org/melvo/xmpp-providers (some automatisation like this might be good, although IBR should not be listed so high as in this example.) <-- what is IBR?

## Communication {#postponed-comm}

###  joinjabber or joinxmpp? {#postponed-comm-jabberxmpp}

-  Jabber is trademarked by Cisco and usually associated with the old xmpp of the pre 2010 times or worse some Cisco corporate inhouse chat people dumped for Slack
- XMPP has a uphill battle against tech people associating it with bad "Jabber" experiences in the past. Hence modern XMPP should distance itself as much as possible from the old Jabber
- joinjabber.org should redirect to joinxmpp.org not the other way around like it is now
- joinxmpp.org is fine for what it is i.e. sharing a link that explains modern xmpp and its clients, actual end-user adoption is driven by client and server recommendations, not so much the protocol

### Upstream news to follow {#postponed-comm-upstreamnews}

- [xmpp tag on Lemmy](https://lemmy.ml/c/xmpp)
- [XSF blog on xmpp.org](https://xmpp.org/blog.html)
- [Planet Jabber](https://planet.jabber.org/)

### Useful resources {#postponed-comm-resources}

What external resources do we want to advertise on our website?

- https://modernxmpp.org/
- https://snikket.org/
- https://homebrewserver.club/
- https://omemo.top/
- https://www.freie-messenger.de/sys_xmpp/ (German)
- https://cryptoflausch.de/ (German)
- https://planet.jabber.org/
- https://search.jabber.network/
- other helpful onboarding guides 
- ADD SOMETHING HERE

### Website suggestions {#postponed-comm-websitesuggestions}

- tutorials
- FAQ
- design inspiration from https://join.lemmy.ml/

## Questions and answers {#postponed-questions}

- How do we avoid becoming a central infrastructure in the ecosystem? How do we prevent ourselves from becoming a Single Point Of Failure, despite providing services for many projects?
- Do we plan to take an active part in crowdsourcing of features for XMPP servers/clients?
