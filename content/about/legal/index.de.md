---
title: "Legal"
description: "Credits etc."
weight: 3
---

## Nutzungsbedingungen

Mit Benutzung unserer Angebote stimmst du zu dich an unseren [Code of Conduct](/about/codeofconduct) (leider bisher nur auf Englisch) zu halten.

## Datenschutz

Under Angebot ist in der EU und somit fällt es unter alle relevanten lokalen Datenschutzbestimmungen die die DSGVO (GDPR). Es werden von uns keine nicht-funktionellen Cookies gesetzt oder irgendwelche persönlichen Daten gespeichert.

## Website Attributierung

Der Quellcode der Webseite kann [hier](https://codeberg.org/joinjabber/website-hugo) eingesehen werden.

Sie basiert auf der [Compose theme](https://github.com/onweru/compose), von onweru.

Unsere Seite wird mit [Hugo](https://gohugo.io/) erstellt.

[JoinJabber Logo und Favicon Designs](https://codeberg.org/joinjabber/collective) wurden von Line erstellt und von Guillaume leicht modifiziert.

Die Schriftart im Logo ist ["Hey October"](https://www.dafont.com/hey-october.font) von Syafrizal a.k.a. Khurasan.

undraw_*.svg Vektorzeichnungen sind quelloffene Illustrationen von [unDraw](https://undraw.co/).

Server- und Applogos sind urheberrechtlich von den entsprechenden Projekten geschützt. 

## XMPP Anwendungen

Wir nutzen [Prosody](https://prosody.im) als unser XMPP Server um die Join Jabber Gruppenchat zur Verfügung zu stellen.

Wir nutzen [XMPP-web](https://github.com/nioc/xmpp-web) als XMPP App für anonymen Zugang und Gruppenchatkatalog.
