---
title: "Juridisch"
description: "Credits etc."
weight: 3
---

## Servicevoorwaarden

Door onze diensten te gebruiken gaat u akkoord met onze [Gedragscode]({{< ref "about/community/codeofconduct" >}}).

## Privacybeleid

Onze diensten worden gehost in de EU. Alle relevante lokale privacywetten, zoals de AVG (GDPR), zijn van toepassing. Wij plaatsen geen cookies en slaan geen persoonlijke gegevens op.

## Website

De broncode van de website is [hier](https://codeberg.org/joinjabber/website-hugo) te vinden.

Er is gebruikgemaakt van het thema [Compose](https://github.com/onweru/compose) door onweru.

Om de website op te bouwen gebruiken we [Hugo](https://gohugo.io/).

Het [logo van JoinJabber](https://codeberg.org/joinjabber/collective) is een bijdrage van Line met aanpassingen door Guillaume. De iconen voor website en groepschats zijn gemaakt door Guillaume op basis van het aangepaste design van Line.

Het in het logo gebruikte lettertype is ["Hey October"](https://www.dafont.com/hey-october.font) by Syafrizal a.k.a. Khurasan.

De vectorafbeeldingen zijn open-source illustraties van [unDraw](https://undraw.co/).

Service- en app-logo's zijn auteursrechtelijk beschermd door hun respectievelijke eigenaars. 

## Gebruikte XMPP-software

We gebruiken [Prosody](https://prosody.im) als onze XMPP-server om de JoinJabber groepschats te hosten.

We gebruiken [XMPP-web](https://github.com/nioc/xmpp-web) als web-frontend voor toegang zonder registratie.
