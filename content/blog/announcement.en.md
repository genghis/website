---
author: "JoinJabber"
tags: ["blog","announcement","collective"]
title: "The JoinJabber collective is born"
date: "2021-02-10"
image: "blog/undraw_announcement.svg"
---

Last sunday (2021-02-07), we gathered on a public chatroom to start a new collective. In summary, we intend to be a community centered around the needs and desires of end-users and service providers of the Jabber/XMPP network. The complete notes (minutes) of our meeting can be found [here]({{< ref "about/history/meeting01" >}}).

If you would like to participate in the next meeting, please vote for the next date on our forums (offline). It will take place on the public chatroom on [chat@joinjabber.org](xmpp:chat@joinjabber.org?join). The agenda for the meeting is available [on a pad](https://pad.lqdn.fr/p/joinjabber2). Anyone is invited to submit points to the agenda before the meeting, or at the start of it.

# Objectives

Our goals as a collective could be summarized like this:

- provide up-to-date and user-friendly information on the Jabber ecosystem for end-users looking for a client, or a service provider, as well as support channels (forums & chats) in as many languages as possible
- provide collective services for the entire Jabber/XMPP ecosystem, such as a translation system, website hosting, forums/mailinglists, chatrooms (etc.)
- provide up-to-date resources on how to [selfhost](https://en.wikipedia.org/wiki/Self-hosting_(web_services)) a Jabber/XMPP server and how to configure it to give the best services to end-users
- gather feedback on accessibility, privacy, usability and interoperability of different solutions, to provide constructive criticism to implementers of client and server software, as well as server operators

The complete list of our objectives adopted during the meeting can be found in the [meeting #1 minutes]({{< ref "about/history/meeting01" >}}).

# Working Groups

Our collective operates in English because that is the language we all had in common for this meeting. In order to divide tasks among contributors, and facilitate onboarding of new contributors, we split into smaller working-groups:

- Sysadmin: deploy and maintain services on our infrastructure
- Website: create content for our website, and evolve its design
- Translations: coordinate translations and recruit new translators
- Bridging: evaluate solutions to connect different chatrooms across networks (IRC, Matrix, Jabber/XMPP)
- Media: post content and gather replies on our website's microblogging section, as well as various social media platforms (Mastodon, Movim)

# Call for contributions

JoinJabber is a volunteer-run collective. Everyone who is aligned comfortable with our goals is welcome to contribute to any part of our project. If you don't know how to contribute, here's a few ideas.

## Tutorials and other content

If you have experience using a specific client, or maintaining a Jabber server for a community, you can make a tutorial for others to get started. You can also contribute a FAQ.

## Translations

If you would like to help translate JoinJabber resources into your language, please join the chatroom of the **Translations Working Group**

## Design

If you can help with design, we could use:

- a logo, as a banner and as a square favicon
- some help on the design of our website, without using JavaScript and without making it heavy to load
- some help with the design of existing Jabber/XMPP clients (GTK, Qt, ncurses, Android, iOS, webdesign)

## UX

If you are involved in UX and usability studies, you could review the services deployed by our collective and the clients we recommend, as well as the additional services setup by specific server operators, and formulate proposals to improve things.

## Security

If you are a security/pentesting expert, you could help with:

- auditing [our infrastructure](https://codeberg.org/joinjabber/infra)
- auditing Jabber/XMPP servers and the configurations we recommend in various tutorials, as well as prepackaged solutions we advertise like [Yunohost](https://yunohost.org/) and [Snikket](https://snikket.org/)
- auditing Jabber/XMPP [clients we recommend]({{< ref "docs/apps" >}})
- auditing [service providers we recommend on our homepage]({{< ref "docs/servers" >}})

## Sysadmin

If you are a sysadmin/devops familiar with self-hosting services and Ansible (or other declarative/programmable infrastructure solutions), you may contribute to [our infrastructure](https://codeberg.org/joinjabber/infra).

Even if you are not familiar with [Ansible](https://en.wikipedia.org/wiki/Ansible_(software)), you can review our configuration templates for:

- [our web services](https://codeberg.org/joinjabber/infra/src/branch/main/roles/webserver/files)
- [our internal mail services](https://codeberg.org/joinjabber/infra/src/branch/main/roles/mailserver/templates)
- [our Jabber/XMPP multi-user chat server](https://codeberg.org/joinjabber/infra/src/branch/main/roles/jabberserver/files/prosody.cfg.lua)

Other systems administration issues you can contribute to may also be found on [our infrastructure's bugtracker](https://codeberg.org/joinjabber/infra/issues).
