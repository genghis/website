---
author: "JoinJabber"
tags: ["blog","website","collective"]
title: "We joined the Fediverse"
date: "2023-02-13"
image: "blog/undraw_social.svg"
---

By now you might have noticed that small [Mastodon](https://joinmastodon.org/) logo at the top menu of our new website. Yes, [JoinJabber](https://joinjabber.org) officially joined the [Fediverse](https://fediverse.party/)!

It took a while to find a suitable Mastodon instance that was willing to take us in, as due to the recent Twitter exodus many instances had to close registrations to not overload their servers. And of course, true to our [own goals](/about/goals/) we did not want to further centralisation tendencies of XMPP related Fediverse accounts (way too many projects on [fosstodon.org](https://fosstodon.org/) already). So in the end we are happy to have found a cozy place with [indieweb.social](https://indieweb.social/).

Our and their mission statement also seem to overlap nicely:

> INDIEWEB.SOCIAL - building the federated, open, decentralized future

So to make it short, please follow us on our new Fediverse presence at: 

[https://indieweb.social/@joinjabber](https://indieweb.social/@joinjabber)

Thanks to indieweb.social for hosting us!


