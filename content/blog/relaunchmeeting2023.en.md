---
author: "JoinJabber"
tags: ["blog","meeting","collective"]
title: "2023 relaunch meeting"
date: "2023-01-23"
image: "blog/undraw_meeting.svg"
---

Last Friday (2023-01-20), we met on our [public chatroom](xmpp:chat@joinjabber.org?join) to discussed renewed activities of our collective. Sadly 2022 has been a bit of a lull phase for us, but we hope to change this for 2023 and beyond.

One of the main topics was our new [Jabber/XMPP user support channel](xmpp:support@joinjabber.org?join) and other preparations for this year's [FOSDEM conference (4-5. Februrary 2023)](https://fosdem.org/2023/).

Another topic was the long proposed re-design and relaunch of our website as some of the current contributors were not happy with the design and ease of contributions for the initial design first put up quickly in 2021.

The complete notes (minutes) of our meeting can be found [here]({{< ref "about/history/meeting03" >}}).
