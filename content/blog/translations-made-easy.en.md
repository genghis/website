---
author: "JoinJabber"
tags: ["blog", "collective", "translations"]
title: "Translations made easy"
date: "2023-02-23"
image: "blog/undraw_community.svg"
---

After the recent [website redesign]({{< relref "blog/newwebsitehugo" >}}),
we're happy to announce that we've made it possible to translate the website
via [Weblate](https://translate.codeberg.org/projects/joinjabber/website)!

Reaching out to a non-technical crowd is important to us. The piece of
software we use to generate our website ([Hugo](https://gohugo.io)) has
multilingual support, but is unfortunately limited when it comes to
translating the actual content of the website and made it difficult for
anybody to contribute translations without any technical knowledge
(specifically, git).

We are now using [po4a](https://po4a.org) which converts our markdown files to
a different format from the gettext project. Gettext being a very common
translation framework, it has the advantage of giving us many already
available tools to translate our website. Having these other tools available
also makes translations more accessible for non-developers.

[Weblate](https://weblate.org) is a web platform used to translate many Free
Software projects, it understands gettext, and hopefully many of you will be
familiar with it already. You can head right away to [our JoinJabber
project](https://translate.codeberg.org/projects/joinjabber/website) on
Codeberg! Of course you can also send us patches the regular way on the
[repository](https://codeberg.org/joinjabber/website-hugo).

If you want to help translating this website, please come
{{< chatlink "chat" >}}talk to us{{< /chatlink >}}!
