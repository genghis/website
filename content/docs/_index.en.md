---
title: "Get started"
weight: 1
aliases: ["/get-started/"]
---

There are only two easy steps:

1. Register an account on a server
2. Pick a client app

<!-- Find a way to make a server selection wizard for easy on-boarding? -->

{{< button "servers/" "Find a server" "mb-1" >}}

{{< button "apps/" "Pick an app" >}}

Congratulations! You can now open your client and give it your Jabber address to connect to the server and start chatting with some friends.

Feeling lonely? You can join us in the community chat: {{< chatlink "chat" >}}chat@joinjabber.org (web chat){{< /chatlink >}}.

We also have a friendly support chat: {{< chatlink "support" >}}support@joinjabber.org (web chat){{< /chatlink >}}. You can join the chat without registering an account!

More questions? Read our Frequently Asked Questions:
{{< button "faqs/" "FAQ" "mb-1" >}}
