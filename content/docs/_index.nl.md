---
title: "Aan de slag"
weight: 1
---

Je hoeft slechts twee eenvoudige stappen te volgen:

1. Registreer een account op een server
2. Kies een app voor je besturingssysteem

<!-- Een manier vinden om een server selectie wizard te maken voor eenvoudige on-boarding? -->

{{< button "servers/" "Zoek een server" "mb-1" >}}

{{< button "apps/" "Kies een app" >}}

Gefeliciteerd! Je kunt nu de app openen en je Jabber-adres invoeren om verbinding te maken en te chatten met je vrienden.

Voel je je nog wat eenzaam hier? Sluit je dan aan bij de groepschat van onze gemeenschap: {{< chatlink "chat" >}}chat@joinjabber.org (webchat){{< /chatlink >}}.

We hebben ook een toegankelijke groepschat voor technische ondersteuning: {{< chatlink "support" >}}support@joinjabber.org (webchat){{< /chatlink >}}. Via de webchat kun je vragen stellen zonder een account te registreren!

Meer vragen? Lees dan onze FAQ's:
{{< button "faqs/" "Veelgestelde vragen" "mb-1" >}}
