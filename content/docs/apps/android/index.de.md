---
title: "Android"
---

## [<img src="/images/apps/conversations.svg" style="max-height:30px;height:100%"> Conversations](#conversations-android) {#conversations-android}

Wir empfehlen [Conversations](https://conversations.im). Du kannst diese App auf [F-Droid](https://f-droid.org/en/packages/eu.siacs.conversations/), den freien App-Store für Android herunterladen. Falls du F-Droid noch nicht installiert hast, empfehlen wir es wärmsten sich ein wenig Zeit dafür zu nehmen. Falls du die Entwicklung von Conversations unterstützen möchtest, kannst du die App auch im Google Play Store kaufen oder an das Projekt [spenden](https://conversations.im/#donate). Hier geht's zum Download:

[<img alt="Get it on F-Droid" src="/images/apps/fdroid.png" style="max-height:65px;height:100%">](https://f-droid.org/en/packages/eu.siacs.conversations/)
[<img alt="Get it on Google Play" src="/images/apps/google.png" style="max-height:65px;height:100%">](https://play.google.com/store/apps/details?id=eu.siacs.conversations)

## [<img src="/images/apps/cheogram.svg" style="max-height:30px;height:100%"> Cheogram](#cheogram-android) {#cheogram-android}

[Cheogram](https://cheogram.com/) basiert auf Conversations aber beinhaltet einige weitere Funktionen. Hier geht's zum Download:

[<img alt="Get it on F-Droid" src="/images/apps/fdroid.png" style="max-height:65px;height:100%">](https://f-droid.org/en/packages/com.cheogram.android/)
[<img alt="Get it on Google Play" src="/images/apps/google.png" style="max-height:65px;height:100%">](https://play.google.com/store/apps/details?id=com.cheogram.android.playstore)

## [<img src="/images/apps/blabber.png" style="max-height:30px;height:100%"> Blabber.im](#blabber-android) {#blabber-android}

[Blabber.im](https://blabber.im/en.html) basiert ebenfalls auf Conversations aber versucht es etwas nutzerfreundlicher zu gestalten. Man kann es [auf der Webseite](https://blabber.im/en.html) herunterladen oder von F-Droid beziehen.

_Falls du F-Droid nicht benutzt oder die Google Push Benachrichtigungen benutzen willst, lade die [Google Play APK](https://blabber.im/download-ps.php) direkt herunter anstatt einen App-Store zu benutzen. Dieser Download beinhaltet einen Autoupdatemechanismus damit du keine Sicherheitsupdates oder neue Funktionen verpasst._

[<img alt="Get it on F-Droid" src="/images/apps/fdroid.png" style="max-height:65px;height:100%">](https://f-droid.org/packages/de.pixart.messenger/)


