---
title: "Android"
---

The best way to install Android apps is the [F-Droid store](https://f-droid.org/). If you haven't installed it yet, no problem: It takes less than a minute and is very easy like shown in [this video tutorial](https://youtu.be/o-kqQQqb-Sw). You might see a warning about installing .apk files, but the F-Droid app is very safe to install and use. Once installed, clicking the below buttons should open the installation page directly in the F-Droid app.

## [<img src="/images/apps/conversations.svg" style="max-height:30px;height:100%"> Conversations](#conversations-android) {#conversations-android}

[Conversations](https://conversations.im) is the best known Android client. You can download a gratis version of Conversations on [F-Droid](https://f-droid.org/en/packages/eu.siacs.conversations/). If you want to support the development of Conversations, you can buy it on Google's Play Store, or [donate](https://conversations.im/#donate) to the project. Get it on:

[<img alt="Get it on F-Droid" src="/images/apps/fdroid.png" style="max-height:65px;height:100%">](https://f-droid.org/en/packages/eu.siacs.conversations/)
[<img alt="Get it on Google Play" src="/images/apps/google.png" style="max-height:65px;height:100%">](https://play.google.com/store/apps/details?id=eu.siacs.conversations)

*Note*: [On some Android phones](https://dontkillmyapp.com/?2) the gratis version from F-Droid does not have reliable push-notifications. If your phone is affected by this, either get the paid app from the Playstore or try one of the options mentioned below.

## [<img src="/images/apps/cheogram.svg" style="max-height:30px;height:100%"> Cheogram](#cheogram-android) {#cheogram-android}

[Cheogram](https://cheogram.com/) is a fork of Conversations with a focus on [gateways](/docs/faqs/gateways/) to other services and some additional usability features. Get the latest version on [their website](https://cheogram.com/) or on:

[<img alt="Get it on F-Droid" src="/images/apps/fdroid.png" style="max-height:65px;height:100%">](https://f-droid.org/en/packages/com.cheogram.android/)
[<img alt="Get it on Google Play" src="/images/apps/google.png" style="max-height:65px;height:100%">](https://play.google.com/store/apps/details?id=com.cheogram.android.playstore)

*Note*: If you have issues with push-notifications, try adding `https://cheogram.com/fdroid/repo` as an external source under the Settings -> Repositories menu of your F-droid app. This will give you access to a version that uses the Google FCM notifications service similar to the paid app from the Playstore.

## [<img src="/images/apps/blabber.png" style="max-height:30px;height:100%"> Blabber.im](#blabber-android) {#blabber-android}

[Blabber.im](https://blabber.im/en.html) is another fork of Conversations that aims to be easier to use. Get it [from their website](https://blabber.im/en.html) or on F-Droid.

_If you don't use F-Droid or want to make use of Google's push server for instant notifications, install the [Google Play APK](https://blabber.im/download-ps.php), instead of using a store. It will automatically look for updates once installed, so you won't miss security fixes or new features._

[<img alt="Get it on F-Droid" src="/images/apps/fdroid.png" style="max-height:65px;height:100%">](https://f-droid.org/packages/de.pixart.messenger/)

## [<img src="/images/apps/movim.svg" style="max-height:30px;height:100%"> Movim](#movim-android) {#movim-android}

[Movim](https://movim.eu) is a web-client that works well in mobile browsers like Firefox or Chromium. It can be installed as a [PWA app](https://en.wikipedia.org/wiki/Progressive_web_app) by visiting a [Movim instance](https://join.movim.eu) and selecting the "Add to home screen" option from the menu of your browser. Push notifications can be enabled in the Movim settings and will be delivered through the browser's web-push feature.
