---
title: "GNU/Linux"
---

## [<img src="/images/apps/dino.svg" style="max-height:30px;height:100%"> Dino](#dino-linux) {#dino-linux}

[Dino](https://dino.im/) is a new modern looking Jabber/XMPP app for GNU/Linux. It supports audio & video and small conference calls. Get it from your distribution's package manager or here:

[<img alt="Get it on Flathub" src="/images/apps/flathub.svg" style="max-height:65px;height:100%">](https://flathub.org/apps/im.dino.Dino)

The Flatpak version of Dino also runs well on mobile Linux distributions like [PostmarketOS](https://postmarketos.org/) or [Mobian](https://mobian-project.org/).

## [<img src="/images/apps/gajim.svg" style="max-height:30px;height:100%"> Gajim](#gajim-linux) {#gajim-linux}

[Gajim](https://gajim.org/) is a fully featured Jabber/XMPP desktop app that runs on GNU/Linux. It focusses on a power-user feature set. Get it from your distribution's package manager or here:

[<img alt="Get it on Flathub" src="/images/apps/flathub.svg" style="max-height:65px;height:100%">](https://flathub.org/apps/org.gajim.Gajim)

