---
title: "Mac OS"
---

## [<img src="/images/apps/monal.svg" style="max-height:30px;height:100%"> Monal](#monal-macos) {#monal-macos}

[Monal](https://monal-im.org/) is a modern Jabber/XMPP client for iOS and Mac OS. Get it here:

[<img alt="Download on the App Store" src="/images/apps/apple.png" style="max-height:65px;height:100%">](https://apps.apple.com/app/id1637078500)

## [<img src="/images/apps/beagle.png" style="max-height:30px;height:100%"> Beagle](#beagle-macos) {#beagle-macos}

[Beagle](https://beagle.im/) is a Jabber/XMPP client for Mac OS. Get it here:

[<img alt="Download on the App Store" src="/images/apps/apple.png" style="max-height:65px;height:100%">](https://apps.apple.com/us/app/beagleim-by-tigase-inc/id1445349494)
