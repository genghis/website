---
title: "Mac OS"
---

## [<img src="/images/apps/monal.svg" style="max-height:30px;height:100%"> Monal](#monal-macos) {#monal-macos}

[Monal](https://monal-im.org/) is een moderne Jabber/XMPP-app voor iOS en Mac OS.

[<img alt="Download on the App Store" src="/images/apps/apple.png" style="max-height:65px;height:100%">](https://apps.apple.com/app/id1637078500)

## [<img src="/images/apps/beagle.png" style="max-height:30px;height:100%"> Beagle](#beagle-macos) {#beagle-macos}

[Beagle](https://beagle.im/) is nog een Jabber/XMPP-app voor Mac OS.

[<img alt="Download on the App Store" src="/images/apps/apple.png" style="max-height:65px;height:100%">](https://apps.apple.com/us/app/beagleim-by-tigase-inc/id1445349494)
