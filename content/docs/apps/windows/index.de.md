---
title: "Windows"
---

## [<img src="/images/apps/gajim.svg" style="max-height:30px;height:100%"> Gajim](#gajim-windows) {#gajim-windows}

[Gajim](https://gajim.org/) ist eine Jabber/XMPP Anwendung mit großen Funktionsumfang die auch auf Windows läuft. Du kannst sie von der [offiziellen Webseite](https://gajim.org/download/) herunterladen.
