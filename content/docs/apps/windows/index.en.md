---
title: "Windows"
---

## [<img src="/images/apps/gajim.svg" style="max-height:30px;height:100%"> Gajim](#gajim-windows) {#gajim-windows}

[Gajim](https://gajim.org/) is a fully featured Jabber/XMPP app that runs on Windows. Get it [from their website](https://gajim.org/download/) or here:

[<img alt="Get it from Microsoft" src="/images/apps/microsoft.svg" style="min-height:65px;height:100%">](https://apps.microsoft.com/store/detail/gajim/9PGGF6HD43F9)
