---
title: "Windows"
---

## [<img src="/images/apps/gajim.svg" style="max-height:30px;height:100%"> Gajim](#gajim-windows) {#gajim-windows}

[Gajim](https://gajim.org/) is een volledig uitgeruste Jabber/XMPP-applicatie voor o.a. Windows. Installeer het via [de website](https://gajim.org/download/) of hier:

[<img alt="Get it from Microsoft" src="/images/apps/microsoft.svg" style="min-height:65px;height:100%">](https://apps.microsoft.com/store/detail/gajim/9PGGF6HD43F9)
