---
title: "FAQ"
description: "Frequently Asked Questions"
weight: 4
---

Hier gibt es detailliertere Informationen zum Jabbernetzerk:

- **[Nutzer FAQ](user/)**: Häufige Fragen von typischen Benutzern
- **[Fortgeschrittene FAQ](advanced/)**: Detailliertere Fragen zu Jabber/XMPP
- **[Sicherheits FAQ](security/)**: Spezielle Fragen zur Sicherheit und Privatsphäre
- **[Gateway FAQ](gateways/)**: Fragen zu Chatbrücken und ähnliche Anwendungen
- **[Server Admins FAQ](service/)**: Fragen von Leute die ihren eigenen Server betreiben
