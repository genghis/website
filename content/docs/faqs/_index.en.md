---
title: "FAQ"
description: "Frequently Asked Questions"
weight: 4
---

Here you can find more detailed information about the platform:

- **[Users FAQ](user/)**: common questions for typical users
- **[Advanced FAQ](advanced/)**: more detailed questions about Jabber/XMPP
- **[Security FAQ](security/)**: specific security/privacy related questions
- **[Gateway FAQ](gateways/)**: questions about bridging to other services
- **[Server Admins FAQ](service/)**: common questions if you run your own server
