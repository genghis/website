---
title: "FAQ voor veiligheid"
---

### Hoe kan ik mezelf beschermen tegen mijn server? {#faq-sec-protect}

Los van het vertrouwen dat je hebt in je serverbeheerder kun je extra maatregelen nemen om je privacy te beschermen. Zo zal het verbinden met je server via [Tor](https://torproject.org/) of een [VPN](https://en.wikipedia.org/wiki/VPN) zal je locatie van afschermen van de server en tussenliggende partijen. Bovendien zal het versleutelen van je berichten met [OMEMO](https://en.wikipedia.org/wiki/OMEMO) of [PGP](https://nl.wikipedia.org/wiki/Pretty_Good_Privacy) de inhoud van je berichten afschermen van de server, en de server van de ontvanger.

### Hoe kan een kwaadwillige server mij beïnvloeden? {#faq-sec-server}

Een kwaadaardige server is een groot probleem, net als bij andere gefedereerde netwerken (zoals [de Fediverse](https://en.wikipedia.org/wiki/Fediverse) of e-mail). Een kwaadwillende server kan:

- je onversleutelde berichten lezen
- de lijst van je contacten en groepschats (en andere niet-chatgroepen/gemeenschappen) zien
- een geschiedenis lezen van je eerder ontvangen/verzonden berichten als Message Archive Management is ingeschakeld op de server (meestal maar voor een bepaalde tijd), en je nieuwe inkomende berichten lezen
- voorkomen dat je berichten ontvangt, of dat je berichten worden verstuurd
- berichten sturen naar andere servers/gebruikers en zich daarbij voordoen als jou

Deze lijst kan op het eerste gezicht eng klinken, maar dit veiligheidsmodel is vergelijkbaar in de meeste gefedereerde netwerken. Het concept van federatieve netwerken is dat een server verantwoordelijk is voor veel gebruikers en namens hen kan optreden (vanuit het oogpunt van andere gebruikers).

Een kwaadaardige server betekent niet noodzakelijk een kwaadaardige serverbeheerder. Servers kunnen nu eenmaal worden binnengedrongen. Daarom bevorderen wij ook goede beveiligingspraktijken.

### Wat zijn andere bedreigingen waar ik me bewust van moet zijn? {#faq-sec-other}

Als je server niet gecompromitteerd is, moet je er toch rekening mee houden dat:

- de server van je correspondent gecompromitteerd kan zijn, in welk geval je onversleutelde communicatie met deze correspondent nog steeds kan worden geïnspecteerd/gewijzigd/geblokkeerd
- sommige openbare diensten (zoals chatrooms) het Jabber-adres van ieder lid bekend maken: in dit geval kun je ongevraagde berichten van andere gebruikers ontvangen, zelfs nadat je de chatroom hebt verlaten (in pseudonieme chatrooms kunnen andere gebruikers alleen contact met je opnemen zolang je in de chatroom bent)
- pseudonieme openbare diensten nog steeds je Jabber-adres (JID) zullen onthullen aan hun exploitanten en beheerders; bovendien zullen chatservers die [occupant-id (XEP-0421)](https://xmpp.org/extensions/xep-0421.html) implementeren je een uniek ID geven voor elke groepschat, zelfs als je je aansluit met een andere nickname, zodat andere gebruikers je kunnen herkennen (maar niet identificeren); als je je daartegen wilt beschermen, kun je een andere Jabber/XMPP-account aanmaken om met dergelijke diensten te communiceren.
- het gebruik van vcard avatar-afbeeldingen jou kan identificeren in meerdere pseudonieme groepschats, zelfs als je in elke chat een andere naam gebruikt
- de meeste clients standaard aanwezigheidsinformatie publiceren (`online`, `weg` statusberichten), die gebruikt kan worden om je verschillende identiteiten te koppelen (bijv. wie komt online/offline op hetzelfde moment) of om je agenda te bespioneren; dit kan gemakkelijk worden uitgeschakeld in de meeste Jabber/XMPP-clients, dus aarzel niet om je client-ontwikkelaars te vertellen dat privacy de standaard moet zijn!
- een passieve waarnemer op het netwerk de netwerkactiviteit tussen jou en je dienstverlener kan registreren, en kan proberen je netwerkactiviteit te koppelen aan je pseudonieme identiteit
- een passieve waarnemer op het netwerk kan ontdekken dat je verbinding maakt met een specifieke dienstverlener door je DNS-verzoeken en/of TLS SNI-headers af te luisteren
- een actieve aanvaller op het netwerk kan proberen:
  - een [downgrade-aanval](https://en.wikipedia.org/wiki/Downgrade_attack) om minder veilige communicatie met je serviceprovider af te dwingen
  - zich voor te doen als je serviceprovider met een TLS-certificaat dat is goedgekeurd door een gerenommeerde [certificaatautoriteit](https://nl.wikipedia.org/wiki/Certificaatautoriteit)
- je Jabber/XMPP-client sommige informatie onversleuteld op je systeem kan opslaan, zoals je gebruikersnaam/wachtwoord of een geschiedenis van je berichten; de meeste clients kunnen geconfigureerd worden om dat niet te doen

### Moet ik een dienstverlener kiezen die specifiek gericht is op activisten? {#faq-sec-server}

Als je deelneemt aan politieke activiteiten voor sociale verandering, kun je overwegen om een account te nemen bij een zelfgeorganiseerde, activistenvriendelijke hostingcooperatie. Deze zullen gewoonlijk buitenlandse wettelijke verzoeken om je gegevens af te staan negeren, maar wees je ervan bewust dat ze nog steeds door hun eigen lokale autoriteiten gedwongen kunnen worden om gebruikers te verlinken. Ook moet je weten dat radicale servers de veiligheid van sociale strijd bevorderen, maar actief fascistische sympathisanten, crypto-scammers en andere gebruikers die zich bezighouden met schadelijk gedrag voor onze gemeenschappen zullen weren. Scriptkiddies van de wereld, run alsjeblieft je eigen servers, misschien leer je er nog wat van.

Wanneer je een non-profit dienstverlener gebruikt die geen extra maatregelen neemt om de privacy van zijn gebruikers te beschermen, houd er dan rekening mee dat ze meestal worden gerund door kleine gemeenschappen, die ervoor kunnen kiezen om samen te werken met handhavingsinstanties, omdat ze misschien niet de middelen hebben om in de rechtszaal te vechten en nieuwe servers te plaatsen in het geval van inbeslagname van hardware.

Ten slotte kunnen er voordelen zitten aan een account op een populaire server. Afhankelijk van je dreigingsmodel kan een account op een grote, populaire server helpen je metadata te verbergen in de massa's verbindingen van/naar die server.

### Beveiligingsaudits {#faq-sec-audit}

Het grootste deel van het Jabber/XMPP-ecosysteem is niet geaudit op beveiliging. De versleutelde OMEMO-chats van Conversation werden [geaudit in 2016](https://conversations.im/omemo/audit.pdf). Aan de serverkant hebben [prosody](https://prosody.im/) en [ejabberd](https://ejabberd.im/) geen publieke beveiligingsaudit beschikbaar, maar zijn er in de loop der jaren zeer weinig kwetsbaarheden (afgezien van DoS-vectoren) gevonden ([ejabberd](https://www.cvedetails.com/vulnerability-list/vendor_id-4455/product_id-7709/Process-one-Ejabberd.html), [prosody](https://www.cvedetails.com/vulnerability-list/vendor_id-11422/Prosody.html)).

Als een beveiligingsaudit belangrijk voor je is, voel je dan vrij om beveiligingsaudits voor delen van het ecosysteem te verzorgen of te financieren. In het bijzonder hebben sommige ontwikkelaars van [prosody server](https://prosody.im/) de wens geuit om ge-audit te worden. Als een uitgebreide beveiligingsaudit een vereiste voor je is, en je hebt geen vaardigheden/middelen om daaraan bij te dragen, overweeg dan om alternatieven voor het Jabber/XMPP-ecosysteem te gebruiken, zoals [Signal](https://signal.org/) (laatste audit in 2017), [Briar](https://briarproject.org/) (laatste [audit in 2017](https://briarproject.org/news/2017-beta-released-security-audit/)) of [Threema](https://threema.ch/) (laatste audit 2020).

### Wie ontwikkelt Jabber/XMPP? Zal het geblokkeerd worden in mijn land? {#faq-users-block}

Het XMPP/Jabber ecosysteem wordt ontwikkeld en beheerd door een wereldwijd netwerk van vrijwilligers en kleine organisaties, hoewel er ook grote multinationals en overheden zijn die er op vertrouwen. Op dit moment wonen de meeste vrijwilligers van het vrije XMPP-ecosysteem in Centraal-Europa, maar er zijn bijdragers van over de hele wereld. Hoewel het hoogst onwaarschijnlijk is dat een regering het hele XMPP-protocol (dat voor veel dingen wordt gebruikt) zou blokkeren, is het heel goed mogelijk dat ze specifieke servers blokkeren, of client-verbindingen naar buitenlandse servers blokkeren, zoals bijvoorbeeld het geval is met veel diensten die door China's [Great Firewall](https://en.wikipedia.org/wiki/Great_Firewall) gaan.

Hoewel het onwaarschijnlijk is dat Jabber/XMPP door jouw overheid wordt geblokkeerd, is het wel degelijk mogelijk dat ze je verbindingen monitoren en opnemen, wat informatie is die tegen je kan worden gebruikt.

In de meest extreme gevallen is het mogelijk dat een netwerkbeheerder (of overheidsbevel) het Jabber/XMPP-netwerk volledig blokkeert. In dit geval kan het gebruik van censuur-omzeilingsmechanismen zoals [Tor](https://torproject.org/) je helpen om in contact te blijven. Houd er echter rekening mee dat het omzeilen van overheidscensuur waar jij woont een strafbaar feit kan zijn en dat je problemen kunt krijgen met de lokale autoriteiten als ze erachter komen.
