---
title: "Servers"
description: "Recommended servers"
weight: 2
---

{{< tip >}}
Jabber is [federated](docs/faqs/user/#faq-users-federation), just like email or Mastodon. Many servers connect together to create the Jabber network.

You get to pick a server you like and from there connect to your friends, no matter what server they chose.
{{< /tip >}}

{{< tip "warning" >}}
Jabber addresses (also called JIDs) look just like email addresses. So if you create the account `emma.goldman` on server `example.org`, then your address will be `emma.goldman@example.org`.

Group chats have similar addresses, like `chat@groups.example.org`.
{{< /tip >}}

First you need to decide which of the following categories best describe what you are looking for.

### I would like to open a:

{{< button "personal/" "Personal account" >}}

_This category is for individuals looking for a reliable place to open an account._

### I am interested in a service for my:

{{< button "collective/" "Collective" >}}

_This category is for groups and organisations that need multiple accounts and professional hosting._

### I would like to run my own:

{{< button "selfhosted/" "Self-hosted server" >}}

_This category is for adventurous people willing to put in a bit more work themselves._

### Why are there so many options to chose from?

The Jabber network is a large and decentralized effort run by many people and organizations. There is no central company calling all the shots and protocol development is organized collectively through the [XMPP Standards Foundation](https://xmpp.org/about/xmpp-standards-foundation/). This means that there are a lot of servers to choose from, so it can be a bit overwhelming. There are attempts ([1](https://xmpp.org/software/servers/), [2](https://providers.xmpp.net/), [3](https://list.jabber.at/)) to list all this diversity with varying success, but here at [JoinJabber.org](/) we tried to come up with our own recommendations to help you choose.

### Migrating accounts

You changed or mind or made a mistake when originally selecting a server? Not a big deal! While account portability is still a [work in progress](https://docs.modernxmpp.org/projects/portability/) for most of the Jabber network, there is a website that mostly automates it for you [here](https://migrate.modernxmpp.org/). As it currently requires your user-credentials to function, it would be probably good to change the passwords of both accounts after using this tool.

### Can't I just get an account with you?

Sorry, but we do not provide Jabber accounts ourselves. You can learn more about the reasons why [here](/about).
