---
title: "Servers"
description: "Aanbevolen servers"
weight: 2
---

{{< tip >}}
Jabber is [federatief]({{< ref "docs/faqs/user#faq-users-federation" >}}), net als e-mail of Mastodon. Vele servers zijn met elkaar verbonden om samen het Jabber-netwerk te creëren.

Je kunt een server kiezen die je leuk vindt en van daaruit verbinding maken met je vrienden, ongeacht welke server zij hebben gekozen.
{{< /tip >}}

{{< tip "warning" >}}
Jabber-adressen (ook wel JID's genoemd) lijken op e-mailadressen. Dus als je de account `klaas.vaak` aanmaakt op server `voorbeeld.org`, dan is je adres `klaas.vaak@voorbeeld.org`.

Groepschats hebben vergelijkbare adressen, zoals `chat@groups.voorbeeld.org`.
{{< /tip >}}

Eerst moet je bepalen welke van de volgende categorieën het best beschrijft wat je zoekt.

### Ik wil graag een:

{{< button "personal/" "Persoonlijk account" >}}

_Deze categorie is voor personen die op zoek zijn naar een betrouwbare plaats om een account te creëren._

### Ik ben geïnteresseerd in een dienst voor mijn:

{{< button "collective/" "Collectief" >}}

_Deze categorie is voor groepen en organisaties die meerdere accounts en professionele hosting nodig hebben._

### Ik wil graag mijn eigen server draaien:

{{< button "selfhosted/" "Zelf gehost" >}}

_Deze categorie is voor avontuurlijke types die zelf wat meer werk willen verzetten._

### Waarom zijn er zoveel opties om uit te kiezen?

Het Jabber-netwerk is een grote en gedecentraliseerde inspanning van vele mensen en organisaties. Er is geen centraal bedrijf dat de lakens uitdeelt en de protocolontwikkeling is collectief georganiseerd via de [XMPP Standards Foundation](https://xmpp.org/about/xmpp-standards-foundation/). Dit betekent dat er veel servers zijn om uit te kiezen, dus het kan een beetje overweldigend zijn. Er zijn pogingen ([1](https://xmpp.org/software/servers/), [2](https://providers.xmpp.net/), [3](https://list.jabber.at/)) om al deze diversiteit op te sommen, maar hier bij [JoinJabber.org](/) hebben we geprobeerd om met onze eigen aanbevelingen te komen om je te helpen kiezen.

### Accounts migreren

Ben je van gedachten veranderd of heb je een fout gemaakt bij het selecteren van een server? Geen probleem! Hoewel accountportabiliteit nog steeds [in ontwikkeling](https://docs.modernxmpp.org/projects/portability/) is voor het grootste deel van het Jabber netwerk, is er al een website die het [grotendeels automatiseert](https://migrate.modernxmpp.org/). Aangezien het momenteel je inloggegevens vereist om te functioneren, is het waarschijnlijk goed om de wachtwoorden van beide accounts te wijzigen na gebruik van deze tool.

### Kan ik niet gewoon een account bij jullie krijgen?

Sorry, maar wij verstrekken zelf geen Jabber-accounts. Je kunt [hier]({{< ref "about" >}}) meer te weten komen over de redenen waarom.
