---
title: "Kollektiv"
description: "Gemeinsam genutze Server"
weight: 2
---

Hier stellen wir ein paar empfohlene Jabber/XMPP Anbieter vor, die Zugänge für ganze Gruppen oder Organisationen ermöglichen. Sie ermöglichen es auch die eigene Webdomain für die Jabberadressen zu verwenden wie z.B.: `benutzername@organisation.org`.

## Conversations.im

[<img alt="Conversations domain accounts" src="/images/servers/conversations.svg" style="max-height:60px;height:100%"><span style="color:var(--text);font-weight:bold">&nbsp;&nbsp;Conversations.im</span>](https://account.conversations.im/domain/)

Conversations.im ist das kommerzielle Angebot des Entwicklers der [Conversations App](/docs/apps/android/#conversations-android). Mehr über den Registrierungsprozess und wie du deine eingene Webdomain verbinden kannst lernst du auf ihrer [Website](https://account.conversations.im/domain/). Die Server befinden sich in Deutschland.

## Hot-Chilli

[<img alt="Hot-Chilli Jabber hosting" src="/images/servers/hotchilli.png" style="max-height:60px;height:100%">](https://jabber.hot-chilli.net/jabber-hosting/)

Hot-Chilli wird von einer IT Firma in Deutschland betrieben und der kostenlose XMPP Server existiert sein 2005. Alles weitere erfährst du auf ihrer [Homepage](https://jabber.hot-chilli.net/). Es ist möglich die [eigene Webdomain zu verbinden](https://jabber.hot-chilli.net/how-tos/dns-settings-for-jabber-hosting/). 

## Snikket

[<img alt="Snikket hosting" src="/images/servers/snikket.svg" style="max-height:60px;height:100%">](https://snikket.org/hosting/)

Snikket ist ein Projekt eines der [Prosody](https://prosody.im) Entwickler, welches versucht ein einheitliches Appkonzept und nutzerfreundliches [XMPP Serverhosting](https://snikket.org/hosting/) zu verbinden. Genaueres zu den Zielen [hier](https://snikket.org/about/goals/).

## Tigase

[<img alt="Tigase pricing plans" src="/images/servers/tigase.png" style="max-height:60px;height:100%">](https://tigase.net/pricing-xmpp/)

Die Entwickler des Tigase XMPP server bieten auch professionelles Serverhosting für größere Organisationen an.

**Andere?**

Natürlich ist dies keine komplette Liste und wenn du weitere gute Empfehlungen hast einfach mal in unserem chat: {{< chatlink "chat" >}}chat@joinjabber.org (web chat){{< /chatlink >}} vorbei schauen. 

Fallst du einer der Betreiber der oben genannten Server bist und nicht hier gelistet werden möchtest trete bitte mit uns in Kontakt: {{< chatlink "servers" >}}servers@joinjabber.org (web chat){{< /chatlink >}}.
