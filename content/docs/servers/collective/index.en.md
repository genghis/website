---
title: "Collective"
description: "Collective servers"
weight: 2
---

Here you can find some recommended Jabber/XMPP service providers that offer to host an XMPP server for collective use by a group or organization. They also allow the collective to use its own domain name for Jabber IDs like this: `username@organization.org`.

## Conversations.im

[<img alt="Conversations domain accounts" src="/images/servers/conversations.svg" style="max-height:60px;height:100%"><span style="color:var(--text);font-weight:bold">&nbsp;&nbsp;Conversations.im</span>](https://account.conversations.im/domain/)

Conversations.im is the commercial offer by the developer of the [Conversations App](/docs/apps/android/#conversations-android). Learn more about how to register and link your own domain name on their [website](https://account.conversations.im/domain/). Their services are hosted in Germany.

## Hot-Chilli

[<img alt="Hot-Chilli Jabber hosting" src="/images/servers/hotchilli.png" style="max-height:60px;height:100%">](https://jabber.hot-chilli.net/jabber-hosting/)

Hot-Chilli is run by an IT company from Germany and has been running a free XMPP server since 2005. You can learn more about [their Jabber service here](https://jabber.hot-chilli.net/). It is possible to [connect your own domain name to their XMPP server](https://jabber.hot-chilli.net/how-tos/dns-settings-for-jabber-hosting/). 

## Snikket

[<img alt="Snikket hosting" src="/images/servers/snikket.svg" style="max-height:60px;height:100%">](https://snikket.org/hosting/)

Snikket is a project started by one of the [Prosody](https://prosody.im) developers to provide a unified app experience and [user friendly server hosting](https://snikket.org/hosting/). Learn more about [their goals here](https://snikket.org/about/goals/).

## Tigase

[<img alt="Tigase pricing plans" src="/images/servers/tigase.png" style="max-height:60px;height:100%">](https://tigase.net/pricing-xmpp/)

The developers of the Tigase XMPP server also offer professional server hosting for large organizations.

**Others?**

This is obviously not an exhaustive list and if you have a really good recommendation please [contact us here](xmpp:chat@joinjabber.org?join). 

If you are the server operator of one of these services and disagree with being listed here, please [contact us](xmpp:servers@joinjabber.org?join) as well.

