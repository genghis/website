---
title: "Eigener Server"
description: "Software für den Betrieb eines eigenen Servers"
weight: 4
---

Es ist ebenfalls möglich einen eigenen XMPP Server zu betreiben. Hier findest du einige Optionen dafür. Weite Details auch in unseren [Anleitungen](/tutorials).

Der Homebrew Server Club ist auch eine gute Quelle (allerdings nur auf Englisch) [um mehr zu lernen](https://homebrewserver.club/category/instant-messaging.html).

Falls du planst andere Personen deinen Server mitbenutzen zu lassen, schau dir bitte unseren [Server Covenant](/about/community/covenant) an und vielleicht hilft dir auch unsere [Anleitung wie man einen (halb-)öffentlichen Server betreiben sollte](/tutorials/service/public).

Generell empfehlen wir [Prosody](https://prosody.im/doc) oder [Ejabberd](https://docs.ejabberd.im/) um einen eigenen XMPP Server zu betreiben. Aber diese Optionen sollten es etwas einfacher machen damit zu starten:

## Snikket

[<img alt="Get started" src="/images/servers/snikket.svg" style="max-height:60px;height:100%">](https://snikket.org/service/quickstart/)

Snikket ist ein Projekt eines der [Prosody](https://prosody.im) Entwickler, welches versucht ein einheitliches Appkonzept und nutzerfreundliches XMPP Serverhosting zu verbinden. Genaueres zu den Zielen [hier](https://snikket.org/about/goals/).

## Yunohost

[<img alt="Get started" src="/images/servers/yunohost.svg" style="max-height:100px;height:100%">](https://yunohost.org)

YunoHost ist ein Betriebsystem das zum Ziel hat möglichst einfach einen Server zu administrieren, um so das Betreiben eines eigenen Servers zu demokratisieren. Es hat einen [XMPP server eingebaut](https://yunohost.org/en/XMPP) und ermöglicht die Installation vieler weiterer Anwendungen.

## Uberspace

[<img alt="Get started" src="/images/servers/uberspace.svg" style="max-height:100px;height:100%">](https://uberspace.de/en/)

Falls du etwas zwischen einer fertigen Dienstleistung und einem eigenen Server oder VPS suchst könnte Uberspace eine gute Option sein. Sie haben eine einfache [Anleitung wie man einen Prosody XMPP Server](https://lab.uberspace.de/guide_prosody/) auf Ihrem gemeinsam genutzten System installiert.

**Andere?**

Natürlich ist dies keine komplette Liste und wenn du weitere gute Empfehlungen hast einfach mal in unserem chat: {{< chatlink "chat" >}}chat@joinjabber.org (web chat){{< /chatlink >}} vorbei schauen.
