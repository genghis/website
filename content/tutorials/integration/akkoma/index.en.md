---
title: "Akkoma/Pleroma"
---

How to easily integrate a XMPP server with [Akkoma](https://akkoma.social/) or [Pleroma](https://pleroma.social/). In the following we will concentrate on Akkoma (a better Pleroma fork), but Pleroma should work more or less the same.

### Ejabberd external auth

The easiest way to integrate an [Ejabberd XMPP server](https://www.ejabberd.im/) with Akkoma is to use the external auth script from the [official Akkoma documentation](https://docs.akkoma.dev/stable/configuration/integrations/howto_ejabberd/). It works by directly interacting with the Akkoma http API.

### Akkoma bot for Ejabberd

Another easy to use option is to run a bot on your Akkoma instance that can be interacted with to register accounts via the Ejabberd API. You can find the necessary [bot software here](https://git.mastodont.cat/spla/xmpp.git). The this bot also has some other nice functionality to get details from Ejabberd, so it might be worth running even if another account integration method is used.

### Via LDAP

If you are setting up a new Akkoma or Pleroma instance, you might also consider managing the user accounts via a separate LDAP database as explained in the [official Akkoma documentation here](https://docs.akkoma.dev/stable/configuration/cheatsheet/#ldap). Please refer to our [dedicated LDAP tutorial](/tutorials/integration/ldap) for the XMPP server part.

### Prosody

We are still looking into the best options to directly link Prosody to Akkoma or Pleroma. You might be able to use the same Oauth2 method as explained in the Mastodon integration tutorial or via a http basic auth module as explained in the Friendica tutorial.
