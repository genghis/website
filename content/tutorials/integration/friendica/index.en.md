---
title: "Friendica"
---

How to easily integrate a XMPP server with a [Friendica](https://friendi.ca/) federated blogging system:

### Ejabberd external auth

Friendica includes an official external auth script for the [Ejabberd XMPP server](https://www.ejabberd.im/).

It can be found in the Friendica directory under `/bin/auth_ejabberd.php` and can be directly referenced from the Ejabberd configuration file. Please refer to the [official documentation](https://wiki.friendi.ca/docs/install-ejabberd) for further details.

### Prosody module

The [Prosody XMPP server](https://prosody.im/) can be linked to a Friendica instance via the [auth_http_async](https://modules.prosody.im/mod_auth_http_async.html) module. Add the following to the Prosody configuration file to use Friendica for authentication:

```lua
authentication = "http_async";
http_auth_url = "https://example.com/api/account/verify_credentials.json";
```
(use your Friendica domain instead of example.com)
